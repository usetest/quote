help:
	@echo '  '
	@echo 'Usage:  make [COMMAND...]'
	@echo '  '
	@echo 'Commands:  '
	@echo '  '
	@echo -e '  build \tdocker Build an image from a Dockerfile'
	@echo -e '  start \tdocker Start one or more stopped containers'
	@echo -e '  stop \t\tdocker Stop one or more running containers'
	@echo -e '  up \t\tdocker Create and start containers'
	@echo -e '  ps \t\tdocker List containers'
	@echo -e '  logs \t\tdocker Fetch the logs of a container'
	@echo -e '  down \t\tdocker Stop and remove containers, networks'
	@echo -e '  php \t\tapp run php bash'
	@echo '  '

##################
# Variables
##################

DOCKER_COMPOSE = docker compose -f ./docker/docker-compose.yml
DOCKER_COMPOSE_PHP_FPM_EXEC = ${DOCKER_COMPOSE} exec -u www-data php-fpm
PROJECT_DIRECTORY= api

##################
# Docker compose
##################

build:
	${DOCKER_COMPOSE} build --no-cache --pull

start:
	${DOCKER_COMPOSE} start

stop:
	${DOCKER_COMPOSE} stop

up:
	${DOCKER_COMPOSE} up -d --remove-orphans

ps:
	${DOCKER_COMPOSE} ps

logs:
	${DOCKER_COMPOSE} logs -f

down:
	${DOCKER_COMPOSE} down --rmi=all --remove-orphans


##################
# App
##################

php:
	${DOCKER_COMPOSE} exec -u www-data php-fpm bash


##################
# Database
##################

db_migrate:
	${DOCKER_COMPOSE} exec -u www-data php-fpm bin/console doctrine:migrations:migrate --no-interaction
db_diff:
	${DOCKER_COMPOSE} exec -u www-data php-fpm bin/console doctrine:migrations:diff --no-interaction

##################
# Static code analysis
##################

phpstan:
	${DOCKER_COMPOSE_PHP_FPM_EXEC} vendor/bin/phpstan analyse src tests -c phpstan.neon

deptrac:
	${DOCKER_COMPOSE_PHP_FPM_EXEC} vendor/bin/deptrac analyze deptrac-layers.yaml
	${DOCKER_COMPOSE_PHP_FPM_EXEC} vendor/bin/deptrac analyze deptrac-modules.yaml

cs_fix:
	${DOCKER_COMPOSE_PHP_FPM_EXEC} vendor/bin/php-cs-fixer fix

cs_fix_diff:
	${DOCKER_COMPOSE_PHP_FPM_EXEC} vendor/bin/php-cs-fixer fix --dry-run --diff
