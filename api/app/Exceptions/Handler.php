<?php

declare(strict_types=1);

namespace App\Exceptions;

use App\Module\Auth\Response\AuthErrorMessage\AuthErrorMessage;
use App\Module\Shared\Enum\ErrorTypeEnum;
use App\Module\Shared\Exception\APIException;
use App\Module\Shared\Response\ErrorMessage\ErrorMessage;
use App\Module\Shared\Response\NotFoundMessage;
use App\Module\Shared\Response\ValidatorMessage\ValidatorMessage;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    public function register(): void
    {
        $this->renderable(function (APIException $e) {
            return match (true) {
                $e->getType()->is(ErrorTypeEnum::ValidationError) => ValidatorMessage::getMessage($e->getErrors()),
                $e->getType()->is(ErrorTypeEnum::InternalError) => ErrorMessage::getMessage($e->getMessage()),
                $e->getType()->is(ErrorTypeEnum::AuthError) => AuthErrorMessage::getMessage($e->getMessage()),
                $e->getType()->is(ErrorTypeEnum::NotFoundError) => NotFoundMessage::getMessage(),
                default => ErrorMessage::getMessage(),
            };
        });

        $this->renderable(function (Throwable $e) {
            return ErrorMessage::getMessage();
        });
    }
}
