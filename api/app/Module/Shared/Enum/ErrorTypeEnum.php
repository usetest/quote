<?php

declare(strict_types=1);

namespace App\Module\Shared\Enum;

enum ErrorTypeEnum: string
{
    use EnumIs;

    case ValidationError = 'ValidationError';
    case InternalError = 'InternalError';
    case AuthError = 'AuthError';
    case NotFoundError = 'NotFoundError';
}
