<?php

declare(strict_types=1);

namespace App\Module\Shared\Enum;

use BackedEnum;

trait EnumGet
{
    public static function get(BackedEnum|string $expected): BackedEnum
    {
        return ($expected instanceof self) ? $expected : self::default();
    }

    abstract public static function default(): BackedEnum;
}
