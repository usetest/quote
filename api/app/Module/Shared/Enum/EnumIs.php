<?php

declare(strict_types=1);

namespace App\Module\Shared\Enum;

use BackedEnum;

trait EnumIs
{
    public function is(BackedEnum|string $expected): bool
    {
        $expectedValue = ($expected instanceof BackedEnum) ? $expected->value : $expected;

        return $this->value === $expectedValue;
    }
}
