<?php

declare(strict_types=1);

namespace App\Module\Shared\Response\ErrorMessage;

use App\Module\Shared\Enum\ErrorTypeEnum;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ErrorMessage extends JsonResponse
{
    public function __construct($data = [], $status = 200, $headers = [], $options = 0, $json = false)
    {
        parent::__construct($data, $status, $headers, $options, $json);
    }

    public static function getMessage(
        string $message = '',
        int $code = null,
        ?ErrorTypeEnum $errorTypeEnum = null
    ): self
    {
        return new self([
            'code' => $code ?: Response::HTTP_BAD_REQUEST,
            'type' => $errorTypeEnum->value ?? ErrorTypeEnum::InternalError->value,
            'message' => $message ?: ErrorMessageEnum::DefaultMessage->value,
        ], Response::HTTP_BAD_REQUEST);
    }
}
