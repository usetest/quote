<?php

declare(strict_types=1);

namespace App\Module\Shared\Response\ErrorMessage;

enum ErrorMessageEnum: string
{
    case DefaultMessage = 'Unexpected exception.';
    case NotFoundMessage = 'Not Found.';
}
