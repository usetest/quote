<?php

declare(strict_types=1);

namespace App\Module\Shared\Response\SuccessMessage;

use BackedEnum;
use Illuminate\Http\JsonResponse;

class SuccessMessage extends JsonResponse
{
    public static function getSuccess(BackedEnum|string $message = SuccessMessageEnum::Ok): self
    {
        return new self([
            'success' => true,
            'message' => $message instanceof BackedEnum ? $message->value : $message
        ]);
    }

    public static function getNotSuccess(BackedEnum|string $message = SuccessMessageEnum::Fail): self
    {
        return new self([
            'success' => false,
            'message' => $message instanceof BackedEnum ? $message->value : $message
        ]);
    }
}
