<?php

declare(strict_types=1);

namespace App\Module\Shared\Response\SuccessMessage;

enum SuccessMessageEnum: string
{
    case Ok = 'ok';
    case Fail = 'fail';
}
