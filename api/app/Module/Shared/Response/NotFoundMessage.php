<?php

declare(strict_types=1);

namespace App\Module\Shared\Response;

use App\Module\Shared\Enum\ErrorTypeEnum;
use App\Module\Shared\Response\ErrorMessage\ErrorMessageEnum;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class NotFoundMessage extends JsonResponse
{
    public static function getMessage(): self
    {
        return new self([
            'code' => Response::HTTP_NOT_FOUND,
            'type' => ErrorTypeEnum::NotFoundError->value,
            'message' =>  ErrorMessageEnum::NotFoundMessage->value,
        ], Response::HTTP_NOT_FOUND);
    }
}
