<?php

declare(strict_types=1);

namespace App\Module\Shared\Response\ValidatorMessage;

use App\Module\Shared\Enum\ErrorTypeEnum;
use App\Module\Shared\Response\ValidatorMessage\ValidatorMessageEnum;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ValidatorMessage extends JsonResponse
{
    public static function getMessage(array $errors): self
    {
        return new self([
            'code' => Response::HTTP_BAD_REQUEST,
            'type' => ErrorTypeEnum::ValidationError->value,
            'message' => ValidatorMessageEnum::DefaultMessage->value,
            'errors' => $errors
        ], Response::HTTP_BAD_REQUEST);
    }
}
