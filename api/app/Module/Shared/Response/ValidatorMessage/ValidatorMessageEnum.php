<?php

declare(strict_types=1);

namespace App\Module\Shared\Response\ValidatorMessage;

enum ValidatorMessageEnum: string
{
    case DefaultMessage = 'Invalid request';
}
