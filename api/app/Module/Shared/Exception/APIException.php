<?php

declare(strict_types=1);

namespace App\Module\Shared\Exception;

use App\Module\Shared\Enum\ErrorTypeEnum;
use RuntimeException;
use Throwable;

abstract class APIException extends RuntimeException
{
    public function __construct(
        private readonly ErrorTypeEnum $type,
        string $message = "",
        int $code = 0,
        ?Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }

    public function getType(): ErrorTypeEnum
    {
        return $this->type;
    }
}
