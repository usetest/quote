<?php

declare(strict_types=1);

namespace App\Module\Shared\Exception;

use App\Module\Shared\Enum\ErrorTypeEnum;
use App\Module\Shared\Response\ErrorMessage\ErrorMessageEnum;
use Closure;

class NotFoundException extends APIException
{
    public function __construct()
    {
        parent::__construct(
            type: ErrorTypeEnum::NotFoundError,
            message: ErrorMessageEnum::NotFoundMessage->value
        );
    }

    public static function throw(): Closure
    {
        return static fn() => throw new self();
    }
}
