<?php

declare(strict_types=1);

namespace App\Module\Shared\Exception;

use App\Module\Shared\Enum\ErrorTypeEnum;
use App\Module\Shared\Response\ValidatorMessage\ValidatorMessageEnum;

class ValidatorException extends APIException
{
    public function __construct(
        ValidatorMessageEnum   $message,
        private readonly array $errors
    )
    {
        parent::__construct(ErrorTypeEnum::ValidationError, $message->value);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
