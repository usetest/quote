<?php

declare(strict_types=1);

namespace App\Module\Shared\Http\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PingController extends Controller
{
    public function ping(): JsonResponse
    {
        return new JsonResponse([
            'pong' => true
        ]);
    }

    public function pong(Request $request)
    {
        return $request->user();
    }
}
