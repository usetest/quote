<?php

declare(strict_types=1);

namespace App\Module\Shared\Http\Middleware;

use App\Module\Auth\Exception\AuthException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    protected function unauthenticated($request, array $guards): void
    {
        throw new AuthException();
    }
}
