<?php

declare(strict_types=1);

namespace App\Module\Shared\Factory;

use App\Module\User\Entity\User;
use Illuminate\Support\Facades\Hash;

class ConsoleUser
{
    private static User $user;

    private function __construct()
    {
    }

    public static function get(): User
    {
        $config = config('auth.users.console');

        if (!$user = User::query()->where('login', $config['login'])->first()) {
            $user = User::create([
                'name' => $config['name'],
                'login' => $config['login'],
                'password' => Hash::make($config['password'])
            ]);
        }

        return self::$user = $user;
    }
}
