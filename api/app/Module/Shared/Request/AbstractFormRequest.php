<?php

declare(strict_types=1);

namespace App\Module\Shared\Request;

use App\Module\Shared\Exception\ValidatorException;
use App\Module\Shared\Response\ValidatorMessage\ValidatorMessageEnum;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractFormRequest extends FormRequest
{
    protected function failedValidation(Validator $validator): void
    {
        throw new ValidatorException(
            message: ValidatorMessageEnum::DefaultMessage,
            errors: $validator->errors()->messages()
        );
    }
}
