<?php

declare(strict_types=1);

namespace App\Module\Messenger\Controller;

use App\Module\Messenger\Resource\MessengerDetailResource;
use App\Module\Messenger\UseCase\GetMessenger\GetMessengerHandler;
use App\Module\Messenger\UseCase\GetMessenger\GetMessengerRequest;
use App\Module\Shared\Http\Controller\Controller;

class ApiMessengerController extends Controller
{
    public function getMessenger(GetMessengerHandler $handler, GetMessengerRequest $request): MessengerDetailResource
    {
        return $handler->__invoke($request);
    }
}
