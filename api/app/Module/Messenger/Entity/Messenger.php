<?php

declare(strict_types=1);

namespace App\Module\Messenger\Entity;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Messenger extends Model
{
    use HasUlids, HasFactory;

    public $timestamps = false;
    protected $table = 'messenger_messenger';

    protected $fillable = [
        'type',
        'address'
    ];

    protected function type(): Attribute
    {
        return Attribute::make(
            get: static fn (mixed $v) => MessengerTypeEnum::get($v),
            set: static fn (mixed $v) => MessengerTypeEnum::get($v)->value,
        );
    }
}
