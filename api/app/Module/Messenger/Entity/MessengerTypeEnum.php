<?php

declare(strict_types=1);

namespace App\Module\Messenger\Entity;

use App\Module\Shared\Enum\EnumGet;
use App\Module\Shared\Enum\EnumIs;
use App\Module\Shared\Enum\EnumValues;

enum MessengerTypeEnum: string
{
    use EnumIs, EnumValues, EnumGet;
    case Email = 'email';
    case Telegram = 'telegram';
    case Viber = 'viber';
    case Undefined = 'undefined';

    public static function default(): MessengerTypeEnum
    {
        return self::Undefined;
    }
}
