<?php

declare(strict_types=1);

namespace App\Module\Messenger\UseCase\GetMessenger;

use Illuminate\Foundation\Http\FormRequest;

final class GetMessengerRequest extends FormRequest
{
    public function rules(): array
    {
        return [];
    }
}
