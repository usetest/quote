<?php

declare(strict_types=1);

namespace App\Module\Messenger\UseCase\GetMessenger;

use App\Module\Messenger\Entity\Messenger;
use App\Module\Messenger\Resource\MessengerDetailResource;
use App\Module\Shared\Exception\NotFoundException;

final class GetMessengerHandler
{
    public function __invoke(GetMessengerRequest $request): MessengerDetailResource
    {
        $messenger = Messenger::query()->findOr(
            id: $request->route('messenger'),
            callback: NotFoundException::throw()
        );

        return new MessengerDetailResource($messenger);
    }
}
