<?php

declare(strict_types=1);

namespace App\Module\Messenger\Resource;

use App\Module\Messenger\Entity\Messenger;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Messenger
 */
class MessengerDetailResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            $this->type,
            $this->address,
        ];
    }

}
