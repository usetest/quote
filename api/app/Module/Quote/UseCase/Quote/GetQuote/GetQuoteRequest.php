<?php

declare(strict_types=1);

namespace App\Module\Quote\UseCase\Quote\GetQuote;

use App\Module\Shared\Request\AbstractFormRequest;

final class GetQuoteRequest extends AbstractFormRequest
{
    /**
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [];
    }
}
