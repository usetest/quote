<?php

declare(strict_types=1);

namespace App\Module\Quote\UseCase\Quote\GetQuote;

use App\Module\Quote\Entity\Quote;
use App\Module\Quote\Resource\QuoteDetailResource;
use App\Module\Shared\Exception\NotFoundException;

final class GetQuoteHandler
{
    public function __invoke(GetQuoteRequest $request): QuoteDetailResource
    {
        $id = $request->route('quote');

        $quote = Quote::query()->findOr(
            id: $id,
            callback: NotFoundException::throw()
        );

        return new QuoteDetailResource($quote);
    }
}
