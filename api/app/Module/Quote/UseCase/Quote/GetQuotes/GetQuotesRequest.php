<?php

declare(strict_types=1);

namespace App\Module\Quote\UseCase\Quote\GetQuotes;

use App\Module\Shared\Request\AbstractFormRequest;

final class GetQuotesRequest extends AbstractFormRequest
{
    /**
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [];
    }
}
