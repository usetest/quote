<?php

declare(strict_types=1);

namespace App\Module\Quote\UseCase\Quote\GetQuotes;

use App\Module\Quote\Entity\Quote;
use App\Module\Quote\Resource\QuoteDetailResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

final class GetQuotesHandler
{
    public function __invoke(GetQuotesRequest $request): AnonymousResourceCollection
    {
        $quotes = Quote::all();

        return QuoteDetailResource::collection($quotes);
    }
}
