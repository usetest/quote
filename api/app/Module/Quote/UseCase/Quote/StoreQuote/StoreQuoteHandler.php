<?php

declare(strict_types=1);

namespace App\Module\Quote\UseCase\Quote\StoreQuote;

use App\Module\Quote\Entity\Quote;
use App\Module\Quote\Resource\QuoteDetailResource;
use App\Module\Shared\Response\SuccessMessage\SuccessMessage;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Throwable;

final class StoreQuoteHandler
{
    public function __invoke(StoreQuoteRequest $request): JsonResponse|QuoteDetailResource
    {
        try {
            $data = $request->validated();
            $data['user_id'] = Auth::user()->id;

            $quote = Quote::create($data);
        } catch (Throwable) {
            return SuccessMessage::getNotSuccess(StoreQuoteEnumMessage::NotSuccess->value);
        }

        return new QuoteDetailResource($quote);
    }
}
