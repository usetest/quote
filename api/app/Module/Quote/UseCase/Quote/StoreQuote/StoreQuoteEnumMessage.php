<?php

declare(strict_types=1);

namespace App\Module\Quote\UseCase\Quote\StoreQuote;

enum StoreQuoteEnumMessage: string
{
    case NotSuccess = 'Quote not created.';
    case TextNotUnique = 'Text not unique.';
}
