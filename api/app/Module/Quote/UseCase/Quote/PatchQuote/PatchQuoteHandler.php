<?php

declare(strict_types=1);

namespace App\Module\Quote\UseCase\Quote\PatchQuote;

use App\Module\Quote\Entity\Quote;
use App\Module\Quote\Resource\PatchQuoteDetailResource;
use App\Module\Shared\Exception\NotFoundException;
use App\Module\Shared\Response\SuccessMessage\SuccessMessage;
use Illuminate\Http\JsonResponse;
use Throwable;

final class PatchQuoteHandler
{
    public function __invoke(PatchQuoteRequest $request): JsonResponse|PatchQuoteDetailResource
    {
        $id = $request->route('quote');
        $quote = Quote::query()->findOr(id: $id, callback: NotFoundException::throw());

        try {
            $data = $request->validated();

            $quote->update($data);
        } catch (Throwable)            {
            return SuccessMessage::getNotSuccess(PatchQuoteEnumMessage::NotSuccess->value);
        }

        return new PatchQuoteDetailResource($quote);
    }
}
