<?php

declare(strict_types=1);

namespace App\Module\Quote\UseCase\Quote\PatchQuote;

use App\Module\Shared\Request\AbstractFormRequest;

final class PatchQuoteRequest extends AbstractFormRequest
{
    /**
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [
            'text' => 'required|min:3|unique:quote_quote',
        ];
    }

    /**
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'text.unique' => PatchQuoteEnumMessage::TextNotUnique->value
        ];
    }
}
