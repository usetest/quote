<?php

declare(strict_types=1);

namespace App\Module\Quote\UseCase\Quote\PatchQuote;

enum PatchQuoteEnumMessage: string
{
    case NotSuccess = 'Quote not patched.';
    case TextNotUnique = 'Text not unique.';
}
