<?php

declare(strict_types=1);

namespace App\Module\Quote\Resource;

use App\Module\Quote\Entity\Quote;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Quote
 */
final class PatchQuoteDetailResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'   => $this->id,
            'text' => $this->text,
        ];
    }
}
