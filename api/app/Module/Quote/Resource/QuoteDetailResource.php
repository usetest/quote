<?php

declare(strict_types=1);

namespace App\Module\Quote\Resource;

use App\Module\Quote\Entity\Quote;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Quote
 */
final class QuoteDetailResource extends JsonResource
{
    public function toArray($request): array
    {
        $author = $this->author;

        return [
            'quote' => [
                'id' => $this->id,
                'text' => $this->text,
            ],
            'author' => [
                'id' => $author->id,
                'name' => $author->login
            ],
        ];
    }
}
