<?php

declare(strict_types=1);

namespace App\Module\Quote\Command;

enum CommandEnum: string
{
    case AddQuote = 'add:quote';
}
