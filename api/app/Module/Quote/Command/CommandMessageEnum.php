<?php

declare(strict_types=1);

namespace App\Module\Quote\Command;

enum CommandMessageEnum: string
{
    case Exists = 'Quote already exists';
}
