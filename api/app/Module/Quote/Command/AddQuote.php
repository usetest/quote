<?php

declare(strict_types=1);

namespace App\Module\Quote\Command;

use App\Module\Quote\Entity\Quote;
use App\Module\Shared\Factory\ConsoleUser;
use Illuminate\Console\Command;
use RuntimeException;
use Throwable;

class AddQuote extends Command
{
    /**
     * @var string
     */
    protected $signature = 'add:quote {--text : Text for new quote.}';

    /**
     * @var string
     */
    protected $description = 'Add new quote';

    public function handle(): int
    {
        try {
            $text = $this->option('text');

            if (Quote::query()->where('text', $text)->exists()) {
                throw new RuntimeException(CommandMessageEnum::Exists->value);
            }

            Quote::create([
                'text' => $this->option('text'),
                'user_id' => ConsoleUser::get()->id,
            ]);

            $this->output->success('Quote added successfully!');
        } catch (Throwable $e) {
            $this->output->error('Quote not added: ' . $e->getMessage());
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
