<?php

declare(strict_types=1);

namespace App\Module\Quote\Entity;

use App\Module\User\Entity\User;
use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property User $author
 */
class Quote extends Model
{
    /**
     * @see QuoteFactory
     */
    use HasFactory, HasUlids;

    protected $table = 'quote_quote';
    public $timestamps = false;

    /**
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'text',
        'user_id',
    ];

    /**
     * @return BelongsTo|User
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(related: User::class, foreignKey: 'user_id');
    }
}
