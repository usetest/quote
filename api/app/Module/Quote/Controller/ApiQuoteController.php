<?php

declare(strict_types=1);

namespace App\Module\Quote\Controller;

use App\Module\Quote\Resource\PatchQuoteDetailResource;
use App\Module\Quote\Resource\QuoteDetailResource;
use App\Module\Quote\UseCase\Quote\GetQuote\GetQuoteHandler;
use App\Module\Quote\UseCase\Quote\GetQuote\GetQuoteRequest;
use App\Module\Quote\UseCase\Quote\GetQuotes\GetQuotesHandler;
use App\Module\Quote\UseCase\Quote\GetQuotes\GetQuotesRequest;
use App\Module\Quote\UseCase\Quote\PatchQuote\PatchQuoteHandler;
use App\Module\Quote\UseCase\Quote\PatchQuote\PatchQuoteRequest;
use App\Module\Quote\UseCase\Quote\StoreQuote\StoreQuoteHandler;
use App\Module\Quote\UseCase\Quote\StoreQuote\StoreQuoteRequest;
use App\Module\Shared\Http\Controller\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ApiQuoteController extends Controller
{
    public function getQuotes(GetQuotesHandler $handler, GetQuotesRequest $request): AnonymousResourceCollection
    {
        return $handler->__invoke($request);
    }

    public function getQuote(GetQuoteHandler $handler, GetQuoteRequest $request): JsonResponse|QuoteDetailResource
    {
        return $handler->__invoke($request);
    }

    public function storeQuote(StoreQuoteHandler $handler, StoreQuoteRequest $request): JsonResponse|QuoteDetailResource
    {
        return $handler->__invoke($request);
    }

    public function patchQuote(PatchQuoteHandler $handler, PatchQuoteRequest $request): JsonResponse|PatchQuoteDetailResource
    {
        return $handler->__invoke($request);
    }
}
