<?php

declare(strict_types=1);

namespace App\Module\Auth\Exception;

use App\Module\Auth\Response\AuthErrorMessage\AuthErrorMessageEnum;
use App\Module\Shared\Enum\ErrorTypeEnum;
use App\Module\Shared\Exception\APIException;

class AuthException extends APIException
{
    public function __construct(
        AuthErrorMessageEnum $message = AuthErrorMessageEnum::DefaultMessage
    )
    {
        parent::__construct(ErrorTypeEnum::AuthError, $message->value);
    }
}
