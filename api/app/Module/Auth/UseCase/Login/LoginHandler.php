<?php

declare(strict_types=1);

namespace App\Module\Auth\UseCase\Login;

use App\Module\Auth\Exception\AuthException;
use App\Module\Auth\Resource\TokenResource;
use App\Module\Auth\Response\AuthErrorMessage\AuthErrorMessageEnum;
use Illuminate\Support\Facades\Auth;

final class LoginHandler
{
    public function __invoke(LoginRequest $request): TokenResource
    {
        $credentials = $request->validated();

        if (!Auth::attempt($credentials)) {
            throw new AuthException(AuthErrorMessageEnum::LoginFailedMessage);
        }

        return new TokenResource();
    }
}
