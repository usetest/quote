<?php

declare(strict_types=1);

namespace App\Module\Auth\UseCase\Login;

use Illuminate\Foundation\Http\FormRequest;

final class LoginRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'login' => 'required|min:3',
            'password' => 'required|min:3'
        ];
    }
}
