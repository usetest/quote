<?php

declare(strict_types=1);

namespace App\Module\Auth\UseCase\Register;

use Illuminate\Foundation\Http\FormRequest;

final class RegisterRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'login' => 'required|min:3',
            'name' => 'nullable|min:3',
            'password' => 'required|min:3',
        ];
    }
}
