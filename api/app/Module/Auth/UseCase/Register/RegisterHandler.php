<?php

declare(strict_types=1);

namespace App\Module\Auth\UseCase\Register;

use App\Module\Auth\Exception\AuthException;
use App\Module\Auth\Resource\TokenResource;
use App\Module\Auth\Response\AuthErrorMessage\AuthErrorMessageEnum;
use App\Module\User\Entity\User;
use Illuminate\Support\Facades\Hash;
use Throwable;

final class RegisterHandler
{
    public function __invoke(RegisterRequest $request): TokenResource
    {
        $credentials = $request->validated();
        $credentials['password'] = Hash::make($credentials['password']);

        try {
            $user = User::create($credentials);
        } catch (Throwable) {
            throw new AuthException(AuthErrorMessageEnum::UserNotCreatedMessage);
        }

        return new TokenResource($user);
    }
}
