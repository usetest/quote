<?php

declare(strict_types=1);

namespace App\Module\Auth\Response\AuthErrorMessage;

enum AuthErrorMessageEnum: string
{
    case DefaultMessage = 'Unauthorized request.';
    case LoginFailedMessage = 'Login failed.';
    case RegisterFailedMessage = 'Register failed.';
    case UserNotCreatedMessage = 'User not created';
}
