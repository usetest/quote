<?php

declare(strict_types=1);

namespace App\Module\Auth\Response\AuthErrorMessage;

use App\Module\Shared\Enum\ErrorTypeEnum;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AuthErrorMessage extends JsonResponse
{
    public static function getMessage(string $message = ''): self
    {
        return new self([
            'code' => Response::HTTP_UNAUTHORIZED,
            'type' => ErrorTypeEnum::AuthError->value,
            'message' => $message ?: AuthErrorMessageEnum::DefaultMessage->value,
        ], Response::HTTP_UNAUTHORIZED);
    }
}
