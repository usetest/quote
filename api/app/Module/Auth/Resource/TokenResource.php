<?php

declare(strict_types=1);

namespace App\Module\Auth\Resource;

use App\Module\User\Entity\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

/**
 * @mixin User
 */
class TokenResource extends JsonResource
{
    public function __construct(?User $user = null)
    {
        parent::__construct($user ?: Auth::getUser());
    }

    public function toArray($request): array
    {
        return ['token' => $this->createToken('token')->plainTextToken];
    }
}
