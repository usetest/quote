<?php

declare(strict_types=1);

namespace App\Module\Auth\Controller;

use App\Module\Auth\Resource\TokenResource;
use App\Module\Auth\UseCase\Login\LoginHandler;
use App\Module\Auth\UseCase\Login\LoginRequest;
use App\Module\Shared\Http\Controller\Controller;

class LoginController extends Controller
{
    public function login(LoginHandler $handler, LoginRequest $request): TokenResource
    {
        return $handler->__invoke($request);
    }
}
