<?php

declare(strict_types=1);

namespace App\Module\Auth\Controller;

use App\Module\Auth\Resource\TokenResource;
use App\Module\Auth\UseCase\Register\RegisterHandler;
use App\Module\Auth\UseCase\Register\RegisterRequest;
use App\Module\Shared\Http\Controller\Controller;

class RegisterController extends Controller
{
    public function register(RegisterHandler $handler, RegisterRequest $request): TokenResource
    {
        return $handler->__invoke($request);
    }
}
