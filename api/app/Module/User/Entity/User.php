<?php

declare(strict_types=1);

namespace App\Module\User\Entity;

use Database\Factories\Module\User\Entity\UserFactory;
use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    /**
     * @see UserFactory
     */
    use HasFactory, HasApiTokens, HasUlids;

    public $timestamps = false;
    protected $table = 'user_user';

    /**
     * @var array<int, string>
     */
    protected $fillable = [
        'login',
        'name',
        'password',
    ];

    /**
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];
}
