<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\ShowUser;

use App\Module\User\Entity\User;
use App\Module\User\Resource\UserDetailResource;

final class ShowUserHandler
{
    public function __invoke(User $user): UserDetailResource
    {
        return new UserDetailResource($user);
    }
}
