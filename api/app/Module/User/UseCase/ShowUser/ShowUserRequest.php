<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\ShowUser;

use Illuminate\Foundation\Http\FormRequest;

final class ShowUserRequest extends FormRequest
{
    public function rules(): array
    {
        return [];
    }
}
