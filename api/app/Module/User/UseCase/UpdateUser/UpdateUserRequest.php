<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\UpdateUser;

use App\Module\Shared\Request\AbstractFormRequest;

final class UpdateUserRequest extends AbstractFormRequest
{
    /**
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [
            'login' => 'required|min:3',
            'name' => 'required|min:3',
            'password' => 'required|min:3|max:255'
        ];
    }
}
