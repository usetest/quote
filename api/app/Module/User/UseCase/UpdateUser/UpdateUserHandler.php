<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\UpdateUser;

use App\Module\Shared\Response\SuccessMessage\SuccessMessage;
use App\Module\User\Entity\User;
use App\Module\User\Resource\UserDetailResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Throwable;

final class UpdateUserHandler
{
    public function __invoke(UpdateUserRequest $request, User $user): JsonResponse|UserDetailResource
    {
        try {
            $data = $request->validated();
            $data['password'] = Hash::make($data['password']);
            $user->update($data);
        } catch (Throwable) {
            return SuccessMessage::getNotSuccess(UpdateUserEnumMessage::NotSuccess->value);
        }

        return new UserDetailResource($user);
    }
}
