<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\UpdateUser;

enum UpdateUserEnumMessage: string
{
    case NotSuccess = 'User not updated.';
}
