<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\PatchUser;

use App\Module\Shared\Request\AbstractFormRequest;

final class PatchUserRequest extends AbstractFormRequest
{
    /**
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [
            'login' => 'nullable|min:3',
            'name' => 'nullable|min:3',
            'password' => 'nullable|min:3|max:255'
        ];
    }
}
