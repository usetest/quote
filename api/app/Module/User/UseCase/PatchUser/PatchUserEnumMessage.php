<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\PatchUser;

enum PatchUserEnumMessage: string
{
    case NotSuccess = 'User not patched.';
}
