<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\PatchUser;

use App\Module\Shared\Response\SuccessMessage\SuccessMessage;
use App\Module\User\Entity\User;
use App\Module\User\Resource\PatchUserDetailResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Throwable;

final class PatchUserHandler
{
    public function __invoke(PatchUserRequest $request, User $user): JsonResponse|PatchUserDetailResource
    {
        try {
            $data = $request->validated();

            if (isset($data['password'])) {
                $data['password'] = $data['password'] ? Hash::make($data['password']) : '';
            }

            $user->update($data);
        } catch (Throwable)            {
            return SuccessMessage::getNotSuccess(PatchUserEnumMessage::NotSuccess->value);
        }

        return new PatchUserDetailResource($user, array_keys($data));
    }
}
