<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\DeleteUser;

enum DeleteUserEnumMessage: string
{
    case Success = 'The user was deleted.';
    case Fail = 'There was a problem when deleting a user.';
}
