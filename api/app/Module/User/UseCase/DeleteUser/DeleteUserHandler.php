<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\DeleteUser;

use App\Module\User\Entity\User;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

final class DeleteUserHandler
{
    public function __invoke(User $user): JsonResponse
    {
        try {
            $user->delete();
            $message = DeleteUserEnumMessage::Success;
        } catch (Throwable) {
            $message = DeleteUserEnumMessage::Fail;
        }

        return new JsonResponse(
            data: [
                'success' => DeleteUserEnumMessage::Success === $message,
                'message' => $message->value
            ],
            status: Response::HTTP_OK
        );
    }
}
