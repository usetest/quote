<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\StoreUser;

enum StoreUserEnumMessage: string
{
    case NotSuccess = 'User not created.';
}
