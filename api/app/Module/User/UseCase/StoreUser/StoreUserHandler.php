<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\StoreUser;

use App\Module\Shared\Response\SuccessMessage\SuccessMessage;
use App\Module\User\Entity\User;
use App\Module\User\Resource\UserDetailResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Throwable;

final class StoreUserHandler
{
    public function __invoke(StoreUserRequest $request): JsonResponse|UserDetailResource
    {
        try {
            $data = $request->validated();
            $data['password'] = Hash::make($data['password']);
            $user = User::create($data);
        } catch (Throwable) {
            return SuccessMessage::getNotSuccess(StoreUserEnumMessage::NotSuccess->value);
        }

        return new UserDetailResource($user);
    }
}
