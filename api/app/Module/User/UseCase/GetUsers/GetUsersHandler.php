<?php

declare(strict_types=1);

namespace App\Module\User\UseCase\GetUsers;

use App\Module\User\Entity\User;
use App\Module\User\Resource\UserDetailResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

final class GetUsersHandler
{
    public function __invoke(): AnonymousResourceCollection
    {
        $users = User::all();

        return UserDetailResource::collection($users);
    }
}
