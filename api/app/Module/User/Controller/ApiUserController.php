<?php

declare(strict_types=1);

namespace App\Module\User\Controller;

use App\Module\Shared\Http\Controller\Controller;
use App\Module\User\Entity\User;
use App\Module\User\Resource\PatchUserDetailResource;
use App\Module\User\Resource\UserDetailResource;
use App\Module\User\UseCase\DeleteUser\DeleteUserHandler;
use App\Module\User\UseCase\GetUsers\GetUsersHandler;
use App\Module\User\UseCase\PatchUser\PatchUserHandler;
use App\Module\User\UseCase\PatchUser\PatchUserRequest;
use App\Module\User\UseCase\ShowUser\ShowUserHandler;
use App\Module\User\UseCase\ShowUser\ShowUserRequest;
use App\Module\User\UseCase\StoreUser\StoreUserHandler;
use App\Module\User\UseCase\StoreUser\StoreUserRequest;
use App\Module\User\UseCase\UpdateUser\UpdateUserHandler;
use App\Module\User\UseCase\UpdateUser\UpdateUserRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ApiUserController extends Controller
{
    public function index(GetUsersHandler $handler): JsonResponse|AnonymousResourceCollection
    {
        return $handler->__invoke();
    }

    public function show(ShowUserHandler $handler, ShowUserRequest $request, User $user): JsonResponse|UserDetailResource
    {
        return $handler->__invoke($user);
    }

    public function store(StoreUserHandler $handler, StoreUserRequest $request): JsonResponse|UserDetailResource
    {
        return $handler->__invoke($request);
    }

    public function update(UpdateUserHandler $handler, UpdateUserRequest $request, User $user): JsonResponse|UserDetailResource
    {
        return $handler->__invoke($request, $user);
    }

    public function patch(PatchUserHandler $handler, PatchUserRequest $request, User $user): JsonResponse|PatchUserDetailResource
    {
        return $handler->__invoke($request, $user);
    }

    public function delete(DeleteUserHandler $handler, User $user): JsonResponse
    {
        return $handler->__invoke($user);
    }
}
