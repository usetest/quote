<?php

declare(strict_types=1);

namespace App\Module\User\Resource;

use App\Module\User\Entity\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin User
 */
final class PatchUserDetailResource extends JsonResource
{
    public function __construct($resource, private readonly array $fields = [])
    {
        parent::__construct($resource);
    }

    public function toArray($request): array
    {
        return array_filter(
            [
                'id' => $this->id,
                'login' => $this->login,
            ],
            fn(string $key) => $key === 'id' || in_array($key, $this->fields, true),
            ARRAY_FILTER_USE_KEY
        );
    }
}
