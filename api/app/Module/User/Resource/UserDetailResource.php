<?php

declare(strict_types=1);

namespace App\Module\User\Resource;

use App\Module\User\Entity\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin User
 */
final class UserDetailResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'login' => $this->login,
        ];
    }
}
