<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Contracts\Validation\ValidatesWhenResolved;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Providers\FormRequestServiceProvider as LaravelFormRequestServiceProvider;

class FormRequestServiceProvider extends LaravelFormRequestServiceProvider
{
    public function boot(): void
    {
        $this->app->afterResolving(ValidatesWhenResolved::class, function ($resolved) {
            $resolved->validateResolved();
        });

        $this->app->resolving(FormRequest::class, function ($request, $app) {
            FormRequest::createFrom($app['request'], $request)->setContainer($app);
        });
    }
}
