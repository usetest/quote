<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('quote_messenger', static function (Blueprint $table) {
            $table->ulid('id')->primary();
            $table->foreignUlid('quote_id');
            $table->foreignUlid('messenger_id');
            $table->foreignUlid('user_id');
            $table->timestamp('created_at');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('quote_messenger');
    }
};
