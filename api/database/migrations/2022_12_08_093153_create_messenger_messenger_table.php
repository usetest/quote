<?php

declare(strict_types=1);

use App\Module\Messenger\Entity\MessengerTypeEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('messenger_messenger', static function (Blueprint $table) {
            $table->ulid('id')->primary();
            $table->string('type')->default(MessengerTypeEnum::Undefined->value)->comment('Like telegram, viber, etc.');
            $table->string('address');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('messenger_messenger');
    }
};
