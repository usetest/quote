<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('quote_quote', static function (Blueprint $table) {
            $table->ulid('id')->primary();
            $table->text('text');
            $table->text('user_id')->comment('author');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_quote');
    }
};
