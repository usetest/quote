<?php

declare(strict_types=1);

namespace Database\Factories\Module\Messenger\Entity;

use App\Module\Messenger\Entity\Messenger;
use App\Module\Messenger\Entity\MessengerTypeEnum;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Messenger>
 */
class MessengerFactory extends Factory
{
    protected $model = Messenger::class;

    /**
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => (string) Str::ulid(),
            'type' => $this->faker->randomElement(MessengerTypeEnum::values()),
            'address' => $this->faker->email(),
        ];
    }
}
