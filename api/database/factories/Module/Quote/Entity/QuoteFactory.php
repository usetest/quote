<?php

declare(strict_types=1);

namespace Database\Factories\Module\Quote\Entity;

use App\Module\Quote\Entity\Quote;
use App\Module\User\Entity\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Quote>
 */
class QuoteFactory extends Factory
{
    protected $model = Quote::class;

    /**
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => (string) Str::ulid(),
            'text' => $this->faker->unique()->text(),
            'user_id' => static fn() => User::factory()->create()->id,
        ];
    }
}
