<?php

declare(strict_types=1);

namespace Database\Factories\Module\User\Entity;

use App\Module\User\Entity\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<User>
 */
class UserFactory extends Factory
{
    protected $model = User::class;

    /**
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => (string) Str::ulid(),
            'login' => $this->faker->unique()->name(),
            'name' => $this->faker->name(),
            'password' => UserFactoryPasswordEnum::BasePassword->hash(),
        ];
    }
}
