<?php

declare(strict_types=1);

namespace Database\Factories\Module\User\Entity;

use InvalidArgumentException;

enum UserFactoryPasswordEnum: string
{
    case BasePassword = 'password';
    case TestPassword = 'test';

    public function hash(): string
    {
        return match ($this) {
            self::BasePassword => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            self::TestPassword => '$2y$10$l6TPPAwIVsOs5yEVn4QQmO4N8953VIqEvcYPByIRNpsHra/ZnurGO',
        };
    }

    public static function fromHash(string $hash): self
    {
        foreach (self::cases() as $case) {
            if ($case->value === $hash) {
                return $case;
            }
        }

        throw new InvalidArgumentException('PasswordEnum::fromHash has not found: "' . $hash . '"');
    }
}
