<?php

namespace Tests;

use App\Module\User\Entity\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function mergeAuthHeaders(User $user, array $headers = []): array
    {
        return array_merge($headers, [
            'Authorization' => 'Bearer ' . $user->createToken('token')->plainTextToken,
            'Accept' => 'application/json'
        ]);
    }
}
