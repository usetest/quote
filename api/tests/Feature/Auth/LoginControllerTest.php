<?php

namespace Tests\Feature\Auth;

use App\Module\Auth\Response\AuthErrorMessage\AuthErrorMessage;
use App\Module\User\Entity\User;
use Database\Factories\Module\User\Entity\UserFactoryPasswordEnum;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @covers \App\Module\Auth\Controller\LoginController::login
 */
class LoginControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_ok(): void
    {
        // arrange
        /** @var User $user */
        $user = User::factory()->create();
        $expectResponse = (new JsonResponse($user));

        // act
        $authResponse = $this->post(route('api.v1.login'), [
            'login' => $user->login,
            'password' => UserFactoryPasswordEnum::BasePassword->value
        ]);

        $pingAuthResponse = $this->get(route('api.v1.ping.pong'),
            [
                'Authorization' => 'Bearer ' . $authResponse->json('token'),
                'Accept' => 'application/json'
            ]
        );

        //assert
        $pingAuthResponse->assertContent($expectResponse->getContent());
        $this->assertAuthenticatedAs($user);
    }

    public function test_fail(): void
    {
        // arrange
        User::factory()->create();
        $expectResponse = (AuthErrorMessage::getMessage());

        // act
        $pingAuthResponse = $this->get(route('api.v1.ping.pong'), [
            'Accept' => 'application/json'
        ]);

        //assert
        $pingAuthResponse->assertStatus(Response::HTTP_UNAUTHORIZED);
        $pingAuthResponse->assertContent($expectResponse->getContent());
    }
}
