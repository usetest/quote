<?php

declare(strict_types=1);

namespace Tests\Feature\Messenger;

use App\Module\Messenger\Entity\Messenger;
use App\Module\Messenger\Resource\MessengerDetailResource;
use App\Module\Shared\Response\NotFoundMessage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @covers \App\Module\Messenger\Controller\ApiMessengerController::getMessenger
 */
class ApiMessengerControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_messenger_by_id_success(): void
    {
        // arrange
        $messenger = Messenger::factory()->create();
        $expectedResponse = (new MessengerDetailResource($messenger))->response();

        // act
        $response = $this->get(uri: route('api.v1.messengers.getMessenger', $messenger));

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertContent($expectedResponse->getContent());
    }

    public function test_get_messenger_by_id_with_not_found(): void
    {
        // arrange
        $messenger = Messenger::factory()->make();
        $expectedResponse = NotFoundMessage::getMessage();

        // act
        $response = $this->get(uri: route('api.v1.messengers.getMessenger', $messenger));

        //assert
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertContent($expectedResponse->getContent());
    }
}
