<?php

declare(strict_types=1);

namespace Tests\Feature\Quote\Command;

use App\Module\Quote\Command\CommandEnum;
use App\Module\Quote\Entity\Quote;
use Artisan;
use Illuminate\Console\Command;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AddQuoteTest extends TestCase
{
    use RefreshDatabase;

    public function test_execute_successful(): void
    {
        // arrange
        $quote = Quote::factory()->make();

        // act
        Artisan::call(CommandEnum::AddQuote->value, [
            '--text' => $quote->text
        ]);

        //assert
        $this->assertDatabaseHas(Quote::class, [
            'text' => $quote->text
        ]);
    }

    public function test_execute_failed(): void
    {
        // arrange
        $quote = Quote::factory()->create();
        $expectedResponse = Command::FAILURE;

        // act
        $response = Artisan::call(CommandEnum::AddQuote->value, [
            '--text' => $quote->text
        ]);

        $this->assertEquals($expectedResponse, $response);
    }
}
