<?php

namespace Tests\Feature\Quote;

use App\Module\Quote\Entity\Quote;
use App\Module\Quote\Resource\PatchQuoteDetailResource;
use App\Module\Quote\Resource\QuoteDetailResource;
use App\Module\Quote\UseCase\Quote\StoreQuote\StoreQuoteEnumMessage;
use App\Module\Shared\Response\ValidatorMessage\ValidatorMessage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @covers \App\Module\Quote\Controller\ApiQuoteController
 */
class ApiQuoteControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_list_of_quotes_successful_response(): void
    {
        // arrange
        $count = 10;
        Quote::factory(count: $count)->create();

        // act
        $response = $this->get(uri: route('api.v1.quotes.getQuotes'));

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseCount(Quote::class, $count);
    }

    public function test_empty_quote_list_fail_response(): void
    {
        // arrange
        $count = 10;

        // act
        $response = $this->get(uri: route('api.v1.quotes.getQuotes'));

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $this->assertNotEquals($count, Quote::count());
    }

    public function test_get_quote_details_success_response(): void
    {
        // arrange
        $quote = Quote::factory()->create();
        $expectedResponse = (new QuoteDetailResource($quote))->response();

        // act
        $response = $this->get(uri: route('api.v1.quotes.getQuote', $quote));

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertContent($expectedResponse->getContent());
    }

    public function test_success_create_new_quote(): void
    {
        // arrange
        $quote = Quote::factory()->make();

        // act
        $response = $this->post(
            uri: route('api.v1.quotes.storeQuote'),
            data: ['text' => $quote->text],
            headers: $this->mergeAuthHeaders($quote->author)
        );

        //assert
        $response->assertStatus(Response::HTTP_CREATED);
        $quoteInStore = Quote::query()->where('text', $quote->text)->first();
        $expectedResponse = (new QuoteDetailResource($quoteInStore))->response()->getContent();
        $response->assertContent($expectedResponse);
    }

    public function test_fail_to_create_new_quote(): void
    {
        // arrange
        $quote = Quote::factory()->create();
        $expectResponse = ValidatorMessage::getMessage([
            'text' => [StoreQuoteEnumMessage::TextNotUnique->value]
        ]);

        // act
        $response = $this->post(
            uri: route('api.v1.quotes.storeQuote'),
            data: ['text' => $quote->text],
            headers: $this->mergeAuthHeaders($quote->author)
        );

        //assert
        $re = $response->getContent();
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
        $response->assertContent($expectResponse->getContent());
    }

    public function test_success_patch_quote_data(): void
    {
        // arrange
        $quote = Quote::factory()->create();
        $updatedQuote = Quote::factory()->make([
            'id' => $quote->id,
            'author' => $quote->author,
        ]);
        $expectedResponse = (new PatchQuoteDetailResource($updatedQuote))->response();

        // act
        $response = $this->patch(
            uri: route('api.v1.quotes.patchQuote', $quote),
            data: ['text' => $updatedQuote->text],
            headers: $this->mergeAuthHeaders($quote->author)
        );

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertContent($expectedResponse->getContent());
    }

    public function test_fail_to_patch_Quote_data(): void
    {
        // arrange
        $quote = Quote::factory()->create();
        $expectedResponse = ValidatorMessage::getMessage([
            'text' => [StoreQuoteEnumMessage::TextNotUnique->value]
        ]);
        // act
        $response = $this->patch(
            uri: route('api.v1.quotes.patchQuote', $quote),
            data: ['text' => $quote->text],
            headers: $this->mergeAuthHeaders($quote->author)
        );

        //assert
        $co = $response->getContent();
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
        $response->assertContent($expectedResponse->getContent());
    }
}
