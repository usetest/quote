<?php

namespace Tests\Feature\User;

use App\Module\Shared\Response\SuccessMessage\SuccessMessage;
use App\Module\Shared\Response\ValidatorMessage\ValidatorMessage;
use App\Module\User\Entity\User;
use App\Module\User\Resource\PatchUserDetailResource;
use App\Module\User\Resource\UserDetailResource;
use App\Module\User\UseCase\DeleteUser\DeleteUserEnumMessage;
use App\Module\User\UseCase\PatchUser\PatchUserEnumMessage;
use App\Module\User\UseCase\StoreUser\StoreUserEnumMessage;
use App\Module\User\UseCase\UpdateUser\UpdateUserRequest;
use Database\Factories\Module\User\Entity\UserFactoryPasswordEnum;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @covers \App\Module\User\Controller\ApiUserController
 */
class ApiUserControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_list_of_users_successful_response(): void
    {
        // arrange
        $count = 10;
        User::factory(count: $count)->create();

        // act
        $response = $this->get(uri: route('api.v1.users.index'));

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseCount(User::class, $count);
    }

    public function test_empty_user_list_fail_response(): void
    {
        // arrange
        $count = 10;

        // act
        $response = $this->get(uri: route('api.v1.users.index'));

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $this->assertNotEquals($count, User::count());
    }

    public function test_get_user_details_success_response(): void
    {
        // arrange
        $user = User::factory()->create();
        $expectedResponse = (new UserDetailResource($user))->response();

        // act
        $response = $this->get(uri: route('api.v1.users.show', $user));

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertContent($expectedResponse->getContent());
    }

    public function test_success_create_new_user(): void
    {
        // arrange
        $user = User::factory()->make();

        // act
        $response = $this->post(
            uri: route('api.v1.users.store'),
            data: [
                'login' => $user->login,
                'name' => $user->name,
                'password' => $user->password,
            ]
        );

        //assert
        $response->assertStatus(Response::HTTP_CREATED);
        $userInStore = User::query()->where('login', $user->login)->first();
        $expectedResponse = (new UserDetailResource($userInStore))->response()->getContent();
        $response->assertContent($expectedResponse);
    }

    public function test_fail_to_create_new_user(): void
    {
        // arrange
        $user = User::factory()->create();
        $expectContent = SuccessMessage::getNotSuccess(
            StoreUserEnumMessage::NotSuccess->value
        )->getContent();

        // act
        $response = $this->post(
            uri: route('api.v1.users.store'),
            data: [
                'login' => $user->login,
                'password' => UserFactoryPasswordEnum::BasePassword->value,
            ],
            headers: $this->mergeAuthHeaders($user)
        );

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertContent($expectContent);
    }

    public function test_success_update_user_data(): void
    {
        // arrange
        $user = User::factory()->create();
        $updatedUser = User::factory()->make([
            'id' => $user->id,
            'password' => UserFactoryPasswordEnum::TestPassword->value
        ]);
        $expectedResponse = (new UserDetailResource($updatedUser))->response();
        $expectedPassword = UserFactoryPasswordEnum::fromHash($updatedUser->password)->value;

        // act
        $response = $this->put(
            uri: route('api.v1.users.update', $user),
            data: [
                'name' => $updatedUser->name,
                'login' => $updatedUser->login,
                'password' => $expectedPassword,
            ],
            headers: $this->mergeAuthHeaders($user)
        );

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertContent($expectedResponse->getContent());
        $userInStore = User::query()->find($user->id);
        $this->assertTrue(Hash::check($expectedPassword, $userInStore->password));
    }

    public function test_fail_to_update_user_data(): void
    {
        // arrange
        $user = User::factory()->create();
        $updatedUser = User::factory()->make();

        $payload = ['name' => $updatedUser->name];
        $validator = validator(data: $payload, rules: (new UpdateUserRequest())->rules());
        $expectedResponse = ValidatorMessage::getMessage(
            $validator->errors()->messages()
        );

        // act
        $response = $this->put(
            uri: route('api.v1.users.update', $user),
            data: $payload,
            headers: $this->mergeAuthHeaders($user)
        );

        //assert
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
        $response->assertContent($expectedResponse->getContent());
    }

    public function test_success_patch_user_data(): void
    {
        // arrange
        $user = User::factory()->create();
        $updatedUser = User::factory()->make(['id' => $user->id]);
        $expectedResponse = (new PatchUserDetailResource($updatedUser, ['login']))->response();

        // act
        $response = $this->patch(
            uri: route('api.v1.users.patch', $user),
            data: ['login' => $updatedUser->login],
            headers: $this->mergeAuthHeaders($user)
        );

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertContent($expectedResponse->getContent());
    }

    public function test_fail_to_patch_user_data(): void
    {
        // arrange
        $user = User::factory()->create();
        $expectedResponse = SuccessMessage::getNotSuccess(PatchUserEnumMessage::NotSuccess->value);

        // act
        $response = $this->patch(
            uri: route('api.v1.users.patch', $user),
            data: ['name' => ''],
            headers: $this->mergeAuthHeaders($user)
        );

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertContent($expectedResponse->getContent());
    }

    public function test_success_delete_user(): void
    {
        // arrange
        $user = User::factory()->create();
        $expectedResponse = SuccessMessage::getSuccess(DeleteUserEnumMessage::Success->value);

        // act
        $response = $this->delete(
            uri: route('api.v1.users.delete', $user),
            headers: $this->mergeAuthHeaders($user)
        );

        //assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertContent($expectedResponse->getContent());
        $this->assertNull(User::query()->find($user->uuid));
    }
}
