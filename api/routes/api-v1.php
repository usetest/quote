<?php

declare(strict_types=1);

use App\Module\Auth\Controller\LoginController;
use App\Module\Auth\Controller\RegisterController;
use App\Module\Messenger\Controller\ApiMessengerController;
use App\Module\Quote\Controller\ApiQuoteCollectionController;
use App\Module\Quote\Controller\ApiQuoteController;
use App\Module\Quote\Controller\ApiQuoteMessengerController;
use App\Module\Shared\Http\Controller\PingController;
use App\Module\User\Controller\ApiUserController;
use Illuminate\Support\Facades\Route;

Route::post('register', [RegisterController::class, 'register'])->name('register');
Route::post('login', [LoginController::class, 'login'])->name('login');

Route::controller(PingController::class)
    ->prefix('ping')
    ->name('ping.')
    ->group(static function() {
        Route::get('/ping', 'ping')->name('ping');
        Route::get('/pong', 'pong')->name('pong')->middleware('auth:sanctum');
    });

Route::controller(ApiUserController::class)
    ->prefix('users')
    ->name('users.')
    ->group(static function () {
        Route::get('/', 'index')->name('index');
        Route::get('/{user}', 'show')->name('show');
        Route::post('/', 'store')->name('store');

        Route::middleware('auth:sanctum')->group(static function() {
            Route::put('/{user}', 'update')->name('update');
            Route::patch('/{user}', 'patch')->name('patch');
            Route::delete('/{user}', 'delete')->name('delete');
        });
    });

Route::controller(ApiMessengerController::class)
    ->prefix('messengers')
    ->name('messengers.')
    ->group(
        static fn () => Route::get('/{messenger}', 'getMessenger')->name('getMessenger')
    );

# Quote
Route::controller(ApiQuoteController::class)
    ->prefix('quotes')
    ->name('quotes.')
    ->group(static function() {
        Route::get('/', 'getQuotes')->name('getQuotes');
        Route::get('/{quote}', 'getQuote')->name('getQuote');

        Route::middleware('auth:sanctum')->group(static function() {
            Route::post('/', 'storeQuote')->name('storeQuote');
            Route::patch('/{quote}', 'patchQuote')->name('patchQuote');
        });
    });

# QuoteCollection
Route::controller(ApiQuoteCollectionController::class)
    ->prefix('quote-collections')
    ->name('quote-collections.')
    ->middleware('auth:sanctum')
    ->group(static function() {
        Route::post('/add', 'addQuote')->name('addQuote');
        Route::delete('/remove', 'removeQuote')->name('removeQuote');
    });

# QuoteMessenger
//Route::controller(ApiQuoteMessengerController::class)
//    ->prefix('quote-messengers')
//    ->name('quote-messengers.sendQuote')
//    ->middleware('auth:sanctum')
//    ->post('/send', 'sendQuote');
